import * as express from "express";
import * as socketIo from "socket.io";
import { Server, createServer } from "http";

export class GameServer {

    private players: any = {};
    public static readonly PORT: number = 8081;
    private app: express.Application;
    private server: Server;
    private io: SocketIO.Server;
    private port: string | number;

    constructor() {
        this.createApp();
        this.config();
        this.createServer();
        this.connectClient();
        this.sockets();
        this.listen();
    }

    private createApp(): void {
        this.app = express();
    }

    private config(): void {
        this.port = process.env.PORT || GameServer.PORT;
    }

    private createServer(): void {
        this.server = createServer(this.app);
    }

    private sockets(): void {
        this.io = socketIo(this.server);
    }

    private connectClient(): void {
        this.app.use("/phaser", express.static("node_modules/phaser-ce/build"))
        this.app.use(express.static("public"));
        this.app.get("/", (req, res) => res.sendFile("./index.html"));
    }

    private listen(): void {
        this.server.listen(this.port, () => {
            console.log('Running on port %s.', this.port)
        });

        this.io.on('connection', (socket: socketIo.Socket) => {
            console.log('A user connected.');
            this.createNewPlayer(socket);

            socket.on('disconnect', () => {
                console.log('A user disconnected');
                this.deletePlayer(socket);
            })
        })
    }

    private deletePlayer(socket: socketIo.Socket): void {
        delete this.players[socket.id];
        this.io.emit('disconnect', socket.id);
    }

    private createNewPlayer(socket: socketIo.Socket): void {
        this.players[socket.id] = {
            rotation: 0,
            x: Math.floor(Math.random() * 700) + 50,
            y: Math.floor(Math.random() * 500) + 50,
            playerId: socket.id,
            team: (Math.floor(Math.random() * 2) == 0) ? 'red' : 'blue'
        };
        // send all current players to new player
        socket.emit('currentPlayers', this.players);
        // send new player to all current players
        this.io.emit('disconnect', socket.id);
    }

}

new GameServer();