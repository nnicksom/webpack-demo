import * as socketIo from "socket.io-client"

const SERVER_URL = 'http://localhost:8081';

export class TraitorGame {
  game: Phaser.Game;
  private socket: any;

  constructor() {
    let config = {
      type: Phaser.AUTO,
      parent: "phaser-example",
      width: 800,
      height: 600,
      physics: {
        default: "arcade",
        arcade: {
          debug: false,
          gravity: { y: 0 }
        }
      }
    };

    this.game = new Phaser.Game(config);

    this.preload();
    this.create();
    this.update();
  }

  public preload(): void {/**/ }
  public create(): void {
    this.socket = socketIo(SERVER_URL);
  }
  public update(): void {/**/ }
}